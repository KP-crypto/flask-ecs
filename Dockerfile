FROM python:3.8.2-slim-buster

RUN apt-get update -y 

WORKDIR /app

RUN pip install flask

COPY . /app

EXPOSE 5000

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
